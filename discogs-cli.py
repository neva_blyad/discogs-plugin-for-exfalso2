#!/usr/bin/env python
#
# https://python3-discogs-client.readthedocs.io/en/latest/

import discogs_client
from discogs_client.exceptions import HTTPError
import configparser
from pathlib import Path

import re
import os
import subprocess
import sys

# Show help message and exit
def Usage():
    print("""\
This script downloads music tags from Discogs.com and sets them to all music
files in specified directory.

Warning! Old music tags will be erased.

Usage:  ./discogs-cli.py <DIR>""")
    sys.exit(0)

# Target music directory
if len(sys.argv) != 2:
    Usage();
filepath = sys.argv[1]
if not os.path.isdir(filepath):
    Usage();

# Read config.
#
# Warning! This file stores secret token as plain text.
home = Path('~').expanduser()
cfg_filename = str(home) + '/.config/discogs.cfg'

cfg = configparser.ConfigParser()
cfg.read(cfg_filename)

consumer_key    = None
consumer_secret = None
token           = None
secret          = None

try:
    consumer_key    = cfg['DEFAULT']['consumer_key']
    consumer_secret = cfg['DEFAULT']['consumer_secret']
    token           = cfg['DEFAULT']['token']
    secret          = cfg['DEFAULT']['secret']
except KeyError:
    pass
else:
    pass

# Create an Application if it never was made before.
# The user will act on behalf of their Discogs.com account.
if consumer_key == None or consumer_secret == None:

    # Link header
    url = 'http://www.discogs.com/settings/developers'

    print(f'Go to Discogs API v2 Applications and add the application:')
    print(url)
    print()
    print('Application Name: discogs-plugin-for-exfalso2')
    print(f'Description: Discogs API client for Python')
    print()

    # Waiting for user input
    accepted = 'n'
    while accepted.lower() != 'y':
        print()
        accepted = input(f'Have you done it? [y/N]: ')

    # These credentials are assigned by application and remain static for the
    # lifetime of your discogs application.
    #
    # NOTE: these keys are typically kept SECRET.
    # Here they must enter the verifier key that was provided at the unqiue URL
    # generated above.
    consumer_key    = input('Consumer Key: ')
    consumer_secret = input('Consumer Secret: ')
    print()

# A user-agent is required with Discogs API requests. Be sure to make your
# user-agent unique, or you may get a bad response.
user_agent = 'discogs-plugin-for-exfalso2'

# Instantiate our discogs_client object.
# Prepare the client with our API consumer data.
discogs_client = discogs_client.Client(user_agent)
discogs_client.set_consumer_key(consumer_key, consumer_secret)

# Authenticate without errors
fails    = 0
have_err = None
user     = False

while have_err != False:

    # Try to authenticate
    try:

        # Authorize at Web if needed
        if token == None or secret == None:

            # No token yet.
            # Authorize at Web.
            token, secret, url = discogs_client.get_authorize_url()

            # Prompt your user to "Authorize" the terms of your application.
            # If the user accepts, Discogs displays a key to the user that is used for
            # verification. The key is required in the 2nd phase of authentication.
            print(f'Now browse to the following URL and authorize us:')
            print(url)
            print()

            # Waiting for user input
            accepted = 'n'
            while accepted.lower() != 'y':
                print()
                accepted = input(f'Have you done it? [y/N]: ')

            # Here they must enter the verifier key that was provided at the unqiue URL
            # generated above.
            oauth_verifier = input('Verification code: ')
            token, secret = discogs_client.get_access_token(oauth_verifier)
        else:

            # Token was taken early and now it's saved in config.
            # Just set them.
            discogs_client.set_token(token, secret)

        # Fetch the identity object for the current logged in user
        user = discogs_client.identity()
        have_err = False

    except HTTPError:
        print('Unable to authenticate')
        fails += 1
        if fails == 5:
            sys.exit(1)
        token    = None
        secret   = None
        have_err = True

# Print all the auth data
print(' == User ==')
print(f'    * username           = {user.username}')
print(f'    * name               = {user.name}')
print(' == Access Token ==')
print(f'    * oauth_token        = {token}')
print(f'    * oauth_token_secret = {secret}')
print
print('Authentication complete. Future requests will be signed with the above tokens.')

# Save config
cfg['DEFAULT']['consumer_key']    = consumer_key
cfg['DEFAULT']['consumer_secret'] = consumer_secret
cfg['DEFAULT']['token']           = token
cfg['DEFAULT']['secret']          = secret

with open(cfg_filename, 'w') as desc:
    cfg.write(desc)

# With an active auth token, we're able to do database search
print()
artist = input('Artist [or leave empty]: ')
title  = input('Title  [or leave empty]: ')
label  = input('Label  [or leave empty]: ')

res = discogs_client.search(title, type='release', artist=artist, label=label)

# Print search results
print()
print('== Search results ==')
num = 1
for release in res:

    # Fields
    print()
    print(f'\t== Result #{num} | discogs-id {release.id} ==')
    print(f'\tArtist\t: {", ".join(artist.name for artist in release.artists)}')
    print(f'\tTitle\t: {release.title}')
    print(f'\tYear\t: {release.year}')
    print(f'\tLabels\t: {", ".join(label.name for label in release.labels)}')
    print(f'\tTracknumber\t:')

    for track in release.tracklist:
        tracknumber = track.position
        title       = track.title
        print(f'\t\t{tracknumber}. {title}\t')

    # API image request
    print()
    print('\t\t== Cover ==')
    if len(release.images) > 0:
        cover_url = release.images[0]['uri']
        cover_content, rsp = discogs_client._fetcher.fetch(None,
                                                           'GET',
                                                           cover_url,
                                                           headers={'User-agent': discogs_client.user_agent})
        print(f'\t\t\t* Response status\t: {rsp}')

        if rsp == 200:
            msg = cover_url
        else:
            msg = 'bad response'
    else:
        msg = 'not available'

    print(f'\t\t\t* Primary image URI\t: {msg}')

    num += 1

print()
print('BE CAREFUL! The next operation will clear the tags of all music files in the')
print('specified directory with!')
print('It will also delete all the image files.')

# Release number input
print()
while True:
    num = input(f'You are now entering the number to use tags from. Result: #')
    try:
        num = int(num)
        if num >= 1 and num <= len(res):
            break
    except ValueError:
        pass
release = res[num - 1]

# Read target directory
filenames = []
for (dirpath, dirnames, filenames_) in os.walk(filepath):
    filenames.extend(filenames_)
    break

# Delete any images
print()
print(' == Delete images == ')
for filename in filenames:
    if filename.lower().endswith(('.png', '.jpg', '.jpeg', '.tiff', '.bmp', '.gif')):
        filename = os.path.join(filepath, filename);
        print(filename)

        os.unlink(filename)

# Read target directory
filenames = []
for (dirpath, dirnames, filenames_) in os.walk(filepath):
    filenames.extend(filenames_)
    break

# Download the cover
print()
print(' == Download cover art == ')
if len(release.images) > 0:
    cover_url = release.images[0]['uri']
    cover_content, rsp = discogs_client._fetcher.fetch(None,
                                                       'GET',
                                                       cover_url,
                                                       headers={'User-agent': discogs_client.user_agent})
    print(f'    * Response status:      {rsp}')

    if rsp == 200:
        cover_filename = Path(cover_url.split("/")[-1])
        cover_filepath = os.path.join(filepath, 'Cover' + cover_filename.suffix)

        with open(cover_filepath, 'wb') as desc:
            desc.write(cover_content)

        msg = cover_filepath
    else:
        msg = 'bad response'
else:
    msg = 'not available'

print(f'    * Saving image to disk: {msg}')

# Clear music tags.
# Remove image covers.
print()
print(' == Clear music tags == ')
filenames_music = []
for filename in filenames:
    filename = os.path.join(filepath, filename);
    print(filename)

    code1 = subprocess.run(['operon', 'clear', '-a', filename])
    code2 = subprocess.run(['operon', 'image-clear', filename])
    if code1.returncode == 0 and \
       code2.returncode == 0:
        filenames_music.append(filename)

# Set music tags
print()
print(' == Set music tags ==')
for filename in filenames_music:
    print(filename)

    # album
    tag = 'Album'
    val = release.title
    ret = subprocess.run(['operon', 'add', tag, val, filename])

    # artist
    if len(release.artists) > 0:
        tag = 'Artist'
        val = release.artists[0].name
        ret = subprocess.run(['operon', 'add', tag, val, filename])

    # date
    tag = 'Date'
    val = str(release.year)
    ret = subprocess.run(['operon', 'add', tag, val, filename])

    # label,
    # labelno
    for label in release.labels:
        tag = 'Label'
        val = label.name
        ret = subprocess.run(['operon', 'add', tag, val, filename])

        tag = 'Labelno'
        val = label.catno
        ret = subprocess.run(['operon', 'add', tag, val, filename])

    # genre
    if len(release.styles) > 0:
        tag = 'Genre'
        val = release.styles[0]
        ret = subprocess.run(['operon', 'add', tag, val, filename])

    # title
    # tracknumber
    duration = '0:00'
    ret = subprocess.run(['operon', 'info', filename], capture_output=True)
    if ret.returncode == 0:
        stdout = ret.stdout.decode('utf-8')
        obj = re.search('^\s*Lsength\s*|\s*([0-9]+∶[0-9]+)(?=\s*$)', stdout, re.MULTILINE)
        if obj != None:
            duration = obj.groups(0)[0]
            duration = duration.replace('∶', ':')
    (minutes, seconds) = duration.split(':')
    duration_web = int(minutes) * 60 + int(seconds)

    # TODO: fix different tracks with the same durations

    for track in release.tracklist:
        tracknumber = track.position
        title       = track.title
        duration    = track.duration

        (minutes, seconds) = duration.split(':')
        duration_web = int(minutes) * 60 + int(seconds)

        DIFF_MAX = 1
        if duration_real >= duration_web - DIFF_MAX and \
           duration_real <= duration_web + DIFF_MAX:

            tag = 'title'
            val = title
            ret = subprocess.run(['operon', 'add', tag, val, filename])

            tag = 'tracknumber'
            val = tracknumber
            ret = subprocess.run(['operon', 'add', tag, val, filename])

print('Done!')
